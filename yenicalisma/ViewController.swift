//
//  ViewController.swift
//  yenicalisma
//
//  Created by CTNR on 01/04/2020.
//  Copyright © 2020 CTNR. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lblOyuncu1Score: UILabel!
    @IBOutlet weak var lblOyuncu2Score: UILabel!
    @IBOutlet weak var lblOyuncu1Puan: UILabel!
    @IBOutlet weak var lblOyuncu2Puan: UILabel!
    @IBOutlet weak var imgOyuncu1Durum: UIImageView!
    @IBOutlet weak var imgOyuncu2Durum: UIImageView!
    @IBOutlet weak var imgOyuncu1Zar: UIImageView!
    @IBOutlet weak var imgOyuncu2Zar: UIImageView!
    @IBOutlet weak var lblSetSonuc: UILabel!
    var oyuncuPuanlari = (birinciOyuncuPuani: 0 , ikinciOyuncuPuani: 0)
    var oyuncuScorelari = (birinciOyuncuSkoru: 0 , ikinciOyuncuSkoru: 0)
    var oyuncuSira : Int = 1
    var macSetSayisi : Int = 5
    var suankiSet : Int = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let customBackground = UIImage(named: "arkaPlan") {
            self.view.backgroundColor = UIColor(patternImage: customBackground)
        }
    }
    
    func newGame() {
        oyuncuPuanlari.birinciOyuncuPuani = 0
        oyuncuPuanlari.ikinciOyuncuPuani = 0
        oyuncuScorelari.birinciOyuncuSkoru = 0
        oyuncuScorelari.ikinciOyuncuSkoru = 0
        oyuncuSira = 1
        macSetSayisi = 5
        suankiSet = 1
        lblOyuncu1Score.text = "0"
        lblOyuncu2Score.text = "0"
        lblOyuncu1Puan.text = "0"
        lblOyuncu2Puan.text = "0"
        imgOyuncu1Durum.image = UIImage.init(named: "onay")
        imgOyuncu2Durum.image = UIImage.init(named: "bekle")
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        
        if self.suankiSet > self.macSetSayisi {
            var sonuc:String = ""
                       if self.oyuncuScorelari.birinciOyuncuSkoru > self.oyuncuScorelari.ikinciOyuncuSkoru{
                        sonuc = "Player 1 won!"
                           self.lblSetSonuc.text = "Player 1 won!"
                       } else {
                        sonuc = "Player 2 won!"
                           self.lblSetSonuc.text = "Player 2 won!"
                       }
            let alert = UIAlertController(title: sonuc, message: "Game over. \n \(sonuc) \n You can start again", preferredStyle: UIAlertController.Style.alert)
            let alertNewGameAction = UIAlertAction(title: "New Game", style: UIAlertAction.Style.default) { (_) in
                self.newGame()
            }
            let alertOkAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default)
            alert.addAction(alertNewGameAction)
            alert.addAction(alertOkAction)
            present(alert, animated: true, completion: nil)
                       return
                   }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            let zar1 = arc4random_uniform(6) + 1
            let zar2 = arc4random_uniform(6) + 1
            print("telefon sallandi  \(zar1)   \(zar2)  ")
            
            self.imgOyuncu1Zar.image = UIImage(named: String(zar1))
            self.imgOyuncu2Zar.image = UIImage(named: String(zar2))
        
            self.setSonuc(zar1: Int(zar1), zar2: Int(zar2))
        }
            
        
    }
    
    func setSonuc(zar1: Int , zar2: Int){
        if oyuncuSira == 1  {
            oyuncuPuanlari.birinciOyuncuPuani = zar1 + zar2
            lblOyuncu1Puan.text = String(oyuncuPuanlari.birinciOyuncuPuani)
            imgOyuncu1Durum.image = UIImage(named: "bekle")
            imgOyuncu2Durum.image = UIImage(named: "onay")
            lblSetSonuc.text = "2. player place"
            oyuncuSira = 2
            lblOyuncu2Puan.text = "0"
        } else {
            oyuncuPuanlari.ikinciOyuncuPuani = zar1 + zar2
            lblOyuncu2Puan.text = String(oyuncuPuanlari.ikinciOyuncuPuani)
            imgOyuncu1Durum.image = UIImage(named: "onay")
            imgOyuncu2Durum.image = UIImage(named: "bekle")
            oyuncuSira = 1
            // set tamamlamandi
            
            if oyuncuPuanlari.birinciOyuncuPuani > oyuncuPuanlari.ikinciOyuncuPuani {
                oyuncuScorelari.birinciOyuncuSkoru += 1
                lblSetSonuc.text = " 1st player won the \(suankiSet)th set"
                lblOyuncu1Score.text = String(oyuncuScorelari.birinciOyuncuSkoru)
            } else if oyuncuPuanlari.birinciOyuncuPuani < oyuncuPuanlari.ikinciOyuncuPuani {
                oyuncuScorelari.ikinciOyuncuSkoru += 1
                lblSetSonuc.text = "2st player won the \(suankiSet)th set"
                lblOyuncu2Score.text = String(oyuncuScorelari.ikinciOyuncuSkoru)
            } else {
                lblSetSonuc.text = "Draws set \(suankiSet)"
                suankiSet -= 1
            }
            suankiSet += 1
            
        }
        
    }
}

